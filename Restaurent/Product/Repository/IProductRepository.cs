﻿using ProductApi.Models;
using ProductApi.Models.Dto;

namespace ProductApi.Repository
{
    public interface IProductRepository : IGenericRepository<Product>
    {

    }
}
