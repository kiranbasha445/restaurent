﻿using ProductApi.Database;

namespace ProductApi.Repository
{
    public class UnitOfWork : IUnitOfWork , IDisposable
    {
        private readonly ApplicationDbContext _dbcontext;

        public UnitOfWork(ApplicationDbContext dbcontext)
        {
            _dbcontext = dbcontext;
            Products = new ProductRepository(_dbcontext);
        }

        public IProductRepository Products { get; private set; }

        public async Task CompleteAsync()
        {
            await _dbcontext.SaveChangesAsync();
        }
        public void Dispose()
        {
            _dbcontext.Dispose();
        }
    }
}
