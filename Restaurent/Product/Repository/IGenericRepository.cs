﻿namespace ProductApi.Repository
{
    public interface IGenericRepository<T> where T : class
    {
        Task<IEnumerable<T>> GetAll();
        Task<T> GetById(int id);

        Task<bool> Create(T objEntity);

        Task<bool> Update(T objEntity);

        Task<bool> Delete(T objEntity);

    }
}
