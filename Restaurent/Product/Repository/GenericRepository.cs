﻿using AutoMapper;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;
using ProductApi.Database;
using ProductApi.Models;

namespace ProductApi.Repository
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        private readonly ApplicationDbContext _dbcontext;
        private readonly DbSet<T> _dbset;

        public GenericRepository(ApplicationDbContext dbcontext)
        {
            _dbcontext = dbcontext;
            _dbset = _dbcontext.Set<T>();
        }
        public async virtual Task<bool> Create(T objEntity)
        {
            await _dbset.AddAsync(objEntity);
            return true;
        }

        public async virtual Task<bool> Delete(T objEntity)
        {
            _dbset.Remove(objEntity);
            return true;
        }

        public async virtual Task<IEnumerable<T>> GetAll()
        {
            return await _dbset.ToListAsync();
        }

        public async virtual Task<T> GetById(int id)
        {
            return await _dbset.FindAsync(id);
        }

        public virtual Task<bool> Update(T objEntity)
        {
            throw new NotImplementedException();
        }
    }
}
