﻿using AutoMapper;
using ProductApi.Database;
using ProductApi.Models;
using ProductApi.Models.Dto;

namespace ProductApi.Repository
{
    public class ProductRepository : GenericRepository<Product>, IProductRepository
    {
        public ProductRepository(ApplicationDbContext dbcontext) : base(dbcontext)
        {
        }
    }
}
