﻿namespace ProductApi.Repository
{
    public interface IUnitOfWork
    {
        IProductRepository Products { get; }

        Task CompleteAsync();
    }
}
