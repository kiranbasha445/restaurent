﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ProductApi.Migrations
{
    public partial class seeding : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "ProductID", "CategoryName", "Description", "ImageUrl", "Name", "Price" },
                values: new object[,]
                {
                    { 1, "Category A", "Description for Product 1", "https://example.com/product1.jpg", "Product 1", 9.99m },
                    { 2, "Category B", "Description for Product 2", "https://example.com/product2.jpg", "Product 2", 19.99m },
                    { 3, "Category A", "Description for Product 3", "https://example.com/product3.jpg", "Product 3", 29.99m },
                    { 4, "Category B", "Description for Product 4", "https://example.com/product4.jpg", "Product 4", 39.99m },
                    { 5, "Category A", "Description for Product 5", "https://example.com/product5.jpg", "Product 5", 49.99m },
                    { 6, "Category B", "Description for Product 6", "https://example.com/product6.jpg", "Product 6", 59.99m },
                    { 7, "Category A", "Description for Product 7", "https://example.com/product7.jpg", "Product 7", 69.99m },
                    { 8, "Category B", "Description for Product 8", "https://example.com/product8.jpg", "Product 8", 79.99m },
                    { 9, "Category A", "Description for Product 9", "https://example.com/product9.jpg", "Product 9", 89.99m },
                    { 10, "Category B", "Description for Product 10", "https://example.com/product10.jpg", "Product 10", 99.99m },
                    { 11, "Category A", "Description for Product 11", "https://example.com/product11.jpg", "Product 11", 109.99m },
                    { 12, "Category B", "Description for Product 12", "https://example.com/product12.jpg", "Product 12", 119.99m }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "ProductID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "ProductID",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "ProductID",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "ProductID",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "ProductID",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "ProductID",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "ProductID",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "ProductID",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "ProductID",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "ProductID",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "ProductID",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "ProductID",
                keyValue: 12);
        }
    }
}
