﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProductApi.Models;
using ProductApi.Models.Dto;
using ProductApi.Repository;

namespace ProductApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        private readonly ResponseDto responseDto;

        public ProductController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;

            responseDto = new ResponseDto();
        }

        [HttpGet]
        public async Task<object> GetProducts()
        {
            var productList = await _unitOfWork.Products.GetAll();
            responseDto.Result = _mapper.Map<List<ProductDto>>(productList);
            return responseDto;
        }

        [HttpGet("id")]
        public async Task<object> GetProduct(int id)
        {
            var product = await _unitOfWork.Products.GetById(id);
            return _mapper.Map<ProductDto>(product);
        }

        [HttpPost]
        public async Task<object> CreateProduct([FromBody] ProductDto productDto)
        {
            var product = _mapper.Map<Product>(productDto);
            var output = await _unitOfWork.Products.Create(product);
            await _unitOfWork.CompleteAsync();
            return product;
        }
    }
}
