﻿using Microsoft.EntityFrameworkCore;
using ProductApi.Models;

namespace ProductApi.Database
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {

        }

        public DbSet<Product> Products { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Product>().HasData(
        new Product { ProductID = 1, Name = "Product 1", Price = 9.99m, Description = "Description for Product 1", CategoryName = "Category A", ImageUrl = "https://example.com/product1.jpg" },
        new Product { ProductID = 2, Name = "Product 2", Price = 19.99m, Description = "Description for Product 2", CategoryName = "Category B", ImageUrl = "https://example.com/product2.jpg" },
        new Product { ProductID = 3, Name = "Product 3", Price = 29.99m, Description = "Description for Product 3", CategoryName = "Category A", ImageUrl = "https://example.com/product3.jpg" },
        new Product { ProductID = 4, Name = "Product 4", Price = 39.99m, Description = "Description for Product 4", CategoryName = "Category B", ImageUrl = "https://example.com/product4.jpg" },
        new Product { ProductID = 5, Name = "Product 5", Price = 49.99m, Description = "Description for Product 5", CategoryName = "Category A", ImageUrl = "https://example.com/product5.jpg" },
        new Product { ProductID = 6, Name = "Product 6", Price = 59.99m, Description = "Description for Product 6", CategoryName = "Category B", ImageUrl = "https://example.com/product6.jpg" },
        new Product { ProductID = 7, Name = "Product 7", Price = 69.99m, Description = "Description for Product 7", CategoryName = "Category A", ImageUrl = "https://example.com/product7.jpg" },
        new Product { ProductID = 8, Name = "Product 8", Price = 79.99m, Description = "Description for Product 8", CategoryName = "Category B", ImageUrl = "https://example.com/product8.jpg" },
        new Product { ProductID = 9, Name = "Product 9", Price = 89.99m, Description = "Description for Product 9", CategoryName = "Category A", ImageUrl = "https://example.com/product9.jpg" },
        new Product { ProductID = 10, Name = "Product 10", Price = 99.99m, Description = "Description for Product 10", CategoryName = "Category B", ImageUrl = "https://example.com/product10.jpg" },
        new Product { ProductID = 11, Name = "Product 11", Price = 109.99m, Description = "Description for Product 11", CategoryName = "Category A", ImageUrl = "https://example.com/product11.jpg" },
        new Product { ProductID = 12, Name = "Product 12", Price = 119.99m, Description = "Description for Product 12", CategoryName = "Category B", ImageUrl = "https://example.com/product12.jpg" }
          );
        }
    }
}
