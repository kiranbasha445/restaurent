﻿using MVC.Models;
using MVC.Services.IServices;
using Newtonsoft.Json;
using System.Text;
using System.Text.Json.Serialization;

namespace MVC.Services
{
    public class BaseService : IBaseService
    {
        private readonly IHttpClientFactory _httpClientFactory;
        public ResponseDto responseModel { get; set; }


        public BaseService(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
            this.responseModel = new ResponseDto();

        }


        public async Task<T> SendAsync<T>(ApiRequest apiRequest)
        {
            var client = _httpClientFactory.CreateClient("RestaurentApi");

            HttpRequestMessage httpRequestMessage = new HttpRequestMessage();
            httpRequestMessage.RequestUri = new Uri(apiRequest.Url);
            httpRequestMessage.Headers.Add("Accept", "application/json");
            client.DefaultRequestHeaders.Clear();

            if (apiRequest.Data != null)
            {
                httpRequestMessage.Content = new StringContent(JsonConvert.SerializeObject(apiRequest.Data), Encoding.UTF8, "application/json");
            }

            switch (apiRequest.ApiType)
            {
                case SD.ApiType.GET:
                        httpRequestMessage.Method = HttpMethod.Get;
                    break;
                case SD.ApiType.POST:
                    httpRequestMessage.Method = HttpMethod.Post;
                    break;
                case SD.ApiType.PUT:
                    httpRequestMessage.Method = HttpMethod.Put;
                    break;
                case SD.ApiType.DELETE:
                    httpRequestMessage.Method = HttpMethod.Delete;
                    break;
            }

            var response = await client.SendAsync(httpRequestMessage);

            var apiContent = await response.Content.ReadAsStringAsync();
            var apiResponseDto = JsonConvert.DeserializeObject<T>(apiContent);
            return apiResponseDto;
        }
    }
}
