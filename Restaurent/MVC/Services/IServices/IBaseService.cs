﻿using MVC.Models;

namespace MVC.Services.IServices
{
    public interface IBaseService
    {
         ResponseDto responseModel { get; set; }
         Task<T> SendAsync<T>(ApiRequest apiRequest);
    }
}
