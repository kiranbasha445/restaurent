﻿using Microsoft.AspNetCore.Mvc;
using static MVC.SD;

namespace MVC.Models
{
    public class ApiRequest
    {
        public ApiType ApiType { get; set; }
        public string Url { get; set; }
        public object Data { get; set; }
        public string AccessToken { get; set; }
    }
}
